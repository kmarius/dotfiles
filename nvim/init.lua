local vim = vim

local fn = vim.fn
local cmd = vim.cmd

vim.g.mapleader = "ö"
vim.g.maplocalleader = "ö"

vim.o.swapfile = false
vim.o.history = 150
vim.o.viminfo = [['100,"500,:1024,/150,s10]]

vim.o.undofile = true
vim.o.undolevels = 1001
vim.o.undoreload = 10000
vim.gnetrw_dirhistmax = 0

vim.o.errorbells = true
vim.o.visualbell = false
vim.o.timeoutlen = 500
vim.o.foldcolumn = "1"

vim.wo.cursorline = true
vim.wo.number = true
vim.wo.signcolumn = "yes"

vim.o.showtabline = 1
vim.o.laststatus = 3
vim.o.colorcolumn = "80"
vim.o.relativenumber = false
vim.o.showcmd = true
vim.o.wrap = false
vim.o.linebreak = true
vim.o.showbreak = "->"
vim.o.display = "lastline"
vim.opt.shortmess:append({a = true, I = true})
vim.o.report = 0
vim.o.scrolloff = 5
vim.o.hlsearch = true
vim.o.incsearch = true
vim.o.showmatch = true

vim.o.directory = "/tmp//"
vim.o.hidden = true
vim.o.backup = false
vim.o.autoread = true
vim.o.magic = true
vim.o.modeline = true

vim.o.background = "dark"
vim.opt.matchpairs:append({"(:)", "{:}", "[:]", "<:>"})

vim.opt.formatoptions:append({"troqnl1j"})

vim.o.switchbuf = "usetab"
vim.o.confirm = true
vim.o.bs = "indent,eol,start"
vim.o.joinspaces = false

vim.o.timeoutlen = 1000
vim.o.ttimeoutlen = 70

vim.o.cindent	= false
vim.o.autoindent = true
vim.o.smartindent = true

vim.o.smarttab = true
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.softtabstop = -1

vim.o.mouse = "a"

vim.o.ignorecase = true
vim.o.smartcase = true

vim.o.lazyredraw = true
vim.o.redrawtime = 300

vim.o.tildeop = true

vim.o.sel = "inclusive"

vim.o.autochdir = true

vim.o.completeopt = "menu,longest,preview"
-- vim.o.completeopt = "menuone,noselect"

vim.o.wildmenu = true
vim.o.wildmode = "full"

vim.g.sh_no_error = 1

cmd "filetype plugin indent on"

local id = vim.api.nvim_create_augroup("startup", {clear = true})
vim.api.nvim_create_autocmd("FocusGained", {
	group = id,
	command = "checktime",
})
vim.api.nvim_create_autocmd({"BufEnter", "WinEnter", "InsertLeave"}, {
	group = id,
	command = "set cursorline",
})
vim.api.nvim_create_autocmd({"WinLeave", "InsertEnter"}, {
	group = id,
	command = "set nocursorline",
})
vim.api.nvim_create_autocmd("BufReadPost", {
	group = id,
	command = [[if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif]],
})
vim.api.nvim_create_autocmd({"BufNewFile,BufRead"}, {
	group = id,
	pattern = "latexmkrc",
	command = "set filetype=perl",
})
vim.api.nvim_create_autocmd({"BufNewFile,BufRead"}, {
	group = id,
	pattern = "*.service",
	command = "set filetype=systemd",
})

vim.api.nvim_create_autocmd("TextYankPost", {
	group = vim.api.nvim_create_augroup("YankHighlight", {clear = true}),
	callback = function() vim.highlight.on_yank() end,
})

if fn.filereadable(".nvimrc") == 1 then
	cmd "source .nvimrc"
end

require("plugins")
require("keymap").setup()
require("commands")
require("fold").setup()
