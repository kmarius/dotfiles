"
" ftplugin/tex.vim
"

setlocal expandtab
setlocal tabstop=2
setlocal shiftwidth=2
setlocal textwidth=99
setlocal colorcolumn=100

" inkscape-figures functions and keybinds
function! InkscapeFiguresCreate()
	silent exec '!mkdir -p ' . b:vimtex.root . '/figures >/dev/null 2>&1'
	silent exec '.!inkscape-figures create "' . getline('.') . '" "' . b:vimtex.root . '/figures/"' "figures/"
	silent exec '!inkscape-figures watch >/dev/null 2>&1 &'
	update
endf

function! InkscapeFiguresEdit()
	silent exec '!inkscape-figures edit "'.b:vimtex.root.'/figures/" >/dev/null 2>&1 &'
	silent exec '!inkscape-figures watch >/dev/null 2>&1 &'
	redraw!
	update
endf

inoremap <c-f> <esc>:call InkscapeFiguresCreate()<cr>
nnoremap <c-f> <esc>:call InkscapeFiguresEdit()<cr>

function! TexClean()
	" remove empty sub/supscripts
	silent! :%s/\s*[_^]\s*{\s*}\s*/ /g

	silent! :%s/\\\?"o/ö/g
	silent! :%s/\\\?"a/ä/g
	silent! :%s/\\\?"u/ü/g
	silent! :%s/\\\?"O/Ö/g
	silent! :%s/\\\?"A/Ä/g
	silent! :%s/\\\?"U/Ü/g
	silent! :%s/\\\?"s/ß/g
endf

augroup texlinter
	au!
	au BufWritePost <buffer> if exists(':Neomake') | Neomake chktex | endif
augroup END

function! TexUmlauts()
	silent! :%s/ö/"o/g
	silent! :%s/ä/"a/g
	silent! :%s/ü/"u/g
	silent! :%s/Ö/"O/g
	silent! :%s/Ä/"A/g
	silent! :%s/Ü/"U/g
	silent! :%s/ß/"s/g
endf

" for bibtex completion with fzf
function! s:last_word(lines)
	return substitute(join(a:lines), '^.* ', '', '')
endfunction

inoremap <buffer><expr> <c-x><c-k> fzf#complete({
			\ 'source':  '~/.local/bin/bib-parse',
			\ 'reducer': function('<sid>last_word'),
			\ 'down': 10})

nnoremap <buffer> <Leader>eb :split ~/Sync/Documents/master.bib<cr>

function Backward(line, file)
	update
	exec 'edit +' . a:line . ' ' . a:file
	normal! zMzv
endfunction
