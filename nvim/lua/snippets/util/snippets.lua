local s = require("luasnip").snippet
local line_to_cursor = require("luasnip.util.util").get_current_line_to_cursor

local function begin_snippet(t, nodes, opts)
	local trig = type(t) == "table" and t.trig or t
	opts = opts or {}
	local old_cond = opts.condition
	if old_cond then
		opts.condition = function() return (line_to_cursor() == trig) and old_cond() end
	else
		opts.condition = function() return (line_to_cursor() == trig) end
	end
	return s(t, nodes, opts)
end

local function regex_snippet(t, nodes, opts)
	t = type(t) == "table" and t or {trig=t}
	t.regTrig = true
	return s(t, nodes, opts)
end

return {
	begin = begin_snippet,
	regex = regex_snippet,
}
