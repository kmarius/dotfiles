local line_to_cursor = require("luasnip.util.util").get_current_line_to_cursor

local function tex_no_backslash()
	return not string.match(line_to_cursor(), "\\%w+$")
end

return {
	tex_no_backslash = tex_no_backslash,
}
