local ls = require("luasnip")
local d = ls.dynamic_node
local i = ls.insert_node
local sn = ls.snippet_node

local function selection_indented(snip, indent)
	indent = indent or ""
	local res = {}
	local selection = snip.env.SELECT_RAW
	if #selection > 0 then
		local ws, line = string.match(selection[1], "^(%s*)(.*)")
		table.insert(res, line)
		for j = 2,#selection do
			table.insert(res, ((selection[j]):gsub("^"..ws, indent)))
		end
	end
	return res
end

local function visual_insert_helper(_, snip, _, opts)
	opts = opts or {}
	opts.indent = opts.indent or ""
	local nodes = {}
	local initial_text = selection_indented(snip, opts.indent)
	if #initial_text > 0 then
		table.insert(nodes, i(1, initial_text))
	elseif opts.initial_text then
		table.insert(nodes, i(1, opts.initial_text))
	else
		table.insert(nodes, i(1))
	end
	return sn(nil, nodes)
end

-- local function helper2(args, _, opts)
-- 	return sn(nil, {i(1, args[1].env.SELECT_DEDENT or {})})
-- 	-- return sn(nil, {i(1)})
-- end

local function visual_insert_node(pos, opts)
	return d(pos, visual_insert_helper, {}, {user_args={opts}})
end

return {
	visual_insert = visual_insert_node,
}
