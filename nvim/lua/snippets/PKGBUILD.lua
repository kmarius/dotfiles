return {
	s("ver", {
		t({
			"pkgver() {",
			[[  cd "$pkgname"]],
			[[  git describe --long | sed 's/\([^-]*-g\)/r\1/;s/-/./g']],
			"}",
		}),
	}),
}
