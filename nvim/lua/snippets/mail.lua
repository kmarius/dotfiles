local shell = require("snippets.util.fn").shell

return {
	-- neomutt attachment using the Attach: pseudo-header
	s("at", {
		f(shell, {}, {
			user_args = {
				[[
				tmp=$(mktemp)
				foot lfm-select "$tmp"
				awk '{gsub(/ /, "\\ ", $0); print "Attach:", $0}END{printf "\n"}' "$tmp"
				rm -f "$tmp"
				]]
			}
		})
	}),
}
