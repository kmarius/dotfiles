return {
	s("if", fmt([[
	if {} then
		{}
	end
	]], {i(1), vi(2, {indent="\t"})})),

	s("ife", fmt([[
	if {} then
		{}
	else
		{}
	end
	]], {i(1), vi(2, {indent="\t"}), i(3)})),

	s("function", fmt([[
	function {}({})
		{}
	end
	]], {i(1), i(2), vi(3, {indent="\t"})})),

	s("fun", fmt([[
	function {}({})
		{}
	end
	]], {i(1), i(2), vi(3, {indent="\t"})})),

	s("lam", fmt([[function({}) {} end]], {i(1), vi(2)})),
	s("lambda", fmt([[function({}) {} end]], {i(1), vi(2)})),
}
