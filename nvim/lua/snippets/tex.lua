local no_backslash = require("snippets.util.conditions").tex_no_backslash
local leading_whitespace = require("snippets.util.fn").leading_whitespace
local capture = require("snippets.util.fn").capture

local function item_indent(args)
	local lnum = args[1].env.TM_LINE_INDEX
	local lines = vim.api.nvim_buf_get_lines(0, 0, lnum, false)
	for i=lnum,1,-1 do
		if string.find(lines[i], "\\item") then
			return leading_whitespace(lines[i])
		elseif string.find(lines[i], "begin.*enumerate") or string.find(lines[i], "begin.*itemize") then
			return leading_whitespace(lines[i]) .. "  "
		elseif string.find(lines[i], "end.*enumerate") or string.find(lines[i], "end.*itemize") then
		return leading_whitespace(lines[i])
	end
end
return ""
end

-- Generate dynamic matrix snippet. Dimensions are taken from the first and
-- second capture groups.
-- Use as: d(1, matrix, {}).
local matrix
do
	-- Generates (rows x cols) pmatrix snippet.
	local function matrix_snippet(rows, cols)
		local nodes = {t({"\\begin{pmatrix}", ""})}
		for k = 1,rows do
			table.insert(nodes, t("\t"))
			for l = 1,cols do
				table.insert(nodes, i(l+(k-1)*cols))
				if l < cols then
					table.insert(nodes, t(" & "))
				end
			end
			if k < rows then
				table.insert(nodes, t({"\\\\", ""}))
			end
		end
		table.insert(nodes, t({"", "\\end{pmatrix}"}))
		return sn(nil, nodes)
	end

	function matrix(text, snip)
		local captures = snip.captures
		return matrix_snippet(tonumber(captures[1]), tonumber(captures[2]))
	end
end

local snippets = {
	s({trig="int"}, {t("\\int_{"), i(1), t("}^{"), i(2), t("}")}),
	s({trig="intt"}, {t("\\int_{"), i(1), t("}^{"), i(2), t("}"), i(3), t("\\,d "), i(0)}),
	s({trig="(%d+)x(%d+)", regTrig=true}, {d(1, matrix, {})}),
	s({trig="%s*it", regTrig=true}, {f(item_indent, {}), t("\\item ")}),
	s({trig="head"}, {
		t({
			"\\documentclass[12pt,a4paper]{article}",
			"",
			"\\usepackage[utf8]{inputenc}",
			"\\usepackage[ngerman]{babel}",
			""
		}),
		i(1),
		t({"", "\\begin{document}", "", }),
		vi(2, {indent="\t"}),
		t({"", "\\end{document}"
	})}),
	s({trig="geometry"}, {
		t({"\\usepackage[",
		"  hmarginratio=1:1",
		"  a4paper",
		"  bottom=4cm",
		"  top=4cm",
		"  left=2.5cm",
		"  right=2.5cm",
		"  headheight=16pt",
		"]{geometry}"})
	}),
	s({trig="ams"}, {t("\\usepackage{amsmath}", "\\usepackage{amssymb}", "\\usepackage{amsthm}")}),
	s({trig="babel"}, {t("\\usepackage["), i(1, "ngerman"), t("]{babel}")}),
	s({trig="enc"}, {t("\\usepackage["), i(1, "utf8"), t("]{inputenc}")}),
	s({trig="\\liminf"}, {t("\\liminf_{"), i(1, "n\\to\\infty"), t("}")}),
	s({trig="\\limsup"}, {t("\\limsup_{"), i(1, "n\\to\\infty"), t("}")}),
	s({trig="liminf"}, {t("\\liminf_{"), i(1, "n\\to\\infty"), t("}")}),
	s({trig="limsup"}, {t("\\limsup_{"), i(1, "n\\to\\infty"), t("}")}),
	s({trig="\\lim"}, {t("\\lim_{"), i(1, "n\\to\\infty"), t("}")}),
	s({trig="lim"}, {t("\\lim_{"), i(1, "n\\to\\infty"), t("}")}),
	s({trig="pd"}, {t("\\frac{\\partial "), i(1), t("}{\\partial "), i(2), t("}")}),
	s({trig="en"}, {i(1, "x"), t("_{"), i(2, "1"), t("}, \\ldots, "), r(1), t("_{"), i(3, "n"), t("}")}),
	s({trig="enum"}, {
		t({"\\begin{enumerate}", "  \\item "}),
		vi(1, {}),
		t({"", "\\end{enumerate}"}),
	}),
	s({trig="item"}, {
		t({"\\begin{itemize}", "  \\item "}),
		vi(1, {}),
		t({"", "\\end{itemize}"}),
	}),
	s({trig="la"}, {t("\\langle "), vi(1), t("\\rangle")}),
	s({trig="\\|"}, {t("\\|"), vi(1, {default={"\\cdot"}}), t("\\|")}),
	s({trig="|"}, {t("|"), vi(1, {default={"\\cdot"}}), t("|")}),
	s({trig=","}, {t("\\, ")}),
	s({trig="..."}, {t("\\ldots ")}),
	s({trig=".."}, {t("\\cdots ")}),
	-- s({trig="(%w+)%.", regTrig=true}, {t("\\"), f(capture, {}, {user_args={1}})}),
	s({trig="."}, {t("\\cdot ")}),
	s({trig=":="}, {t("\\coloneqq ")}),
	s({trig=":"}, {t("\\colon ")}),
	s({trig="set"}, {t("\\{"), vi(1), t("\\}")}),
	s({trig="^"}, {t("^{"), vi(1), t("}")}),
	s({trig="_"}, {t("_{"), vi(1), t("}")}),
	s({trig='"'}, {t('"`'), vi(1), t([["']])}),
	s("verb", {t("\\verb+"), vi(1), t("+")}),
	s("\\sqrt{", {t("\\sqrt["), i(1), t("]{")}),
	s("*", {t("^{*}")}),
	s("^-", {t("^{-1}")}),
	s({trig="+"}, {t("\\infty")}),
	-- s({trig="bb(.)", regTrig=true}, {t("\\mathbb{"), f(function(a) return string.upper(a[1].captures[1]) end, {}), t("}")}),
	-- s({trig="mathbb(.)", regTrig=true}, {t("\\mathbb{"), f(function(a) return string.upper(a[1].captures[1]) end, {}), t("}")}),
	-- s({trig="fr(.)", regTrig=true}, {t("\\mathfrak{"), f(capture, {1}, 1), t("}")}),
	-- s({trig="frak(.)", regTrig=true}, {t("\\mathfrak{"), f(capture, {1}, 1), t("}")}),
	-- s({trig="mathfrak(.)", regTrig=true}, {t("\\mathfrak{"), f(capture, {}, 1), t("}")}),
	-- s({trig="cal(.)", regTrig=true}, {t("\\mathcal{"), f(capture, {}, 1), t("}")}),
	-- s({trig="mathcal(.)", regTrig=true}, {t("\\mathcal{"), f(capture, {}, 1), t("}")}),
	s({trig="beg", regTrig=true}, {
		t("\\begin{"), i(1), t({"}", ""}),
		vi(2, {indent="  "}),
		t({"", "\\end{"}), r(1), t("}"),
	}),
	s({trig="begin"}, {
		t("\\begin{"), i(1), t({"}", ""}),
		vi(2, {indent="  "}),
		t({"", "\\end{"}), r(1), t("}"),
	}),
	s({trig="bb"}, {
		t("\\begin{"), i(1), t({"}"}),
		vi(2),
		t({"\\end{"}), r(1), t("}"),
	}),
	s({trig="l"}, {
		t("$"), vi(1), t("$"),
	}),
	s({trig="ll"}, {
		t({"\\[", ""}),
		vi(1, {indent="  "}),
		t({"", "\\]"}),
	}),
}

local recipes = {
	tex_arg0 = {
		trigs = {
			{"max"},
			{"ad"},
			{"Ad"},
			{"alpha", "al"},
			{"beta", "bet"},
			{"bot"},
			{"C"},
			{"cap"},
			{"cdot"},
			{"chi"},
			{"circ", "comp"},
			{"coloneqq"},
			{"const"},
			{"Re", "re"},
			{"Im", "im"},
			{"cos"},
			{"cot"},
			{"cup"},
			{"d"},
			{"ddots", "dd"},
			{"delta", "del"},
			{"Delta", "Del"},
			{"det"},
			{"dim"},
			{"displaystyle", "disp"},
			{"e"},
			{"ell"},
			{"epsilon", "eps"},
			{"equiv", "eq", "=="},
			{"eta"},
			{"exists", "ex"},
			{"exp"},
			{"F"},
			{"forall", "for"},
			{"gamma", "gam"},
			{"Gamma", "Gam"},
			{"ge"},
			{"H"},
			{"id"},
			{"Id"},
			{"in"},
			{"inf"},
			{"infty"},
			{"intprod", "ip"},
			{"iota"},
			{"isom"},
			{"K"},
			{"kappa"},
			{"ker"},
			{"lambda", "lam"},
			{"Lambda", "Lam"},
			{"ldots", "ld"},
			{"le"},
			{"Leftrightarrow", "gdw"},
			{"ln"},
			{"log"},
			{"mapsto"},
			{"mu"},
			{"N"},
			{"nabla", "nab"},
			{"neq"},
			{"noindent"},
			{"not"},
			{"nu"},
			{"ocomp"},
			{"omega", "om"},
			{"Omega", "Om"},
			{"oplus", "o\\+"},
			{"otimes", "ox"},
			{"partial", "part", "dell", "p"},
			{"phi"},
			{"Phi"},
			{"pi"},
			{"Pi"},
			{"pm"},
			{"psi"},
			{"Psi"},
			{"Q"},
			{"qedhere"},
			{"R"},
			{"relax"},
			{"rho"},
			{"Rightarrow", "imp"},
			{"setminus", "setm"},
			{"sigma", "sig"},
			{"Sigma", "Sig", "S"},
			{"sim"},
			{"sin"},
			{"subset", "sub"},
			{"sup"},
			{"supp"},
			{"supset"},
			{"tau"},
			{"textwidth"},
			{"theta", "th"},
			{"Theta", "Th"},
			{"times", "x"},
			{"to"},
			{"tr"},
			{"trans", "tra"},
			{"upsilon", "ups"},
			{"Upsilon", "Ups"},
			{"varepsilon", "veps"},
			{"varphi", "vphi"},
			{"varrho", "vrho"},
			{"vartheta", "vtheta", "vth"},
			{"vdots"},
			{"vfill"},
			{"wedge", "w"},
			{"wp"},
			{"xi"},
			{"Xi"},
			{"Z"},
			{"zeta"},
		},
		gen = function(trig)
			local expanded = "\\"..trig[1]
			for _, alias in pairs(trig) do
				local nodes = {t(expanded)}
				table.insert(snippets, s({trig=alias}, nodes, {condition=no_backslash}))
			end
		end,
	},
	tex_arg0space = {
		trigs = {
			{"floor"},
			{"cap"},
			{"cdot"},
			{"circ"},
			{"coloneqq"},
			{"centering"},
			{"raggedright", "raggedr"},
			{"raggedleft", "raggedl"},
			{"colon"},
			{"quad", "qd"},
			{"qquad", "qqd"},
			{"comp"},
			{"const"},
			{"partial", "part", "dell"},
			{"perp", "orth", "bot"},
			{"cup"},
			{"displaystyle", "disp"},
			{"equiv", "eq"},
			{"exists", "ex"},
			{"forall", "for"},
			{"ge"},
			{"in"},
			{"infty"},
			{"intprod", "ip"},
			{"isom"},
			{"ldots", "ld"},
			{"le"},
			{"mapsto"},
			{"neq"},
			{"noindent", "ni"},
			{"ocomp"},
			{"oplus", "o+"},
			{"otimes", "ox"},
			{"times", "x"},
			{"Leftrightarrow", "gdw"},
			{"Rightarrow", "imp"},
			{"trans", "tra"},
			{"pm"},
			{"sim", "~"},
			{"subset", "sub"},
			{"supset"},
			{"to"},
			{"supp"},
			{"wedge", "w"},
			{"with"},
			{"vdots"},
			{"ddots"},
		},
		gen = function(trig)
			local expanded = "\\"..trig[1].." "
			for _, alias in pairs(trig) do
				local nodes = {t(expanded)}
				table.insert(snippets, s({trig=alias}, nodes))
			end
		end
	},
	tex_arg1 = {
		trigs = {
			{"index", "ind"},
			{"author"},
			{"bar"},
			{"bibitem"},
			{"caption"},
			{"check"},
			{"check"},
			{"cite"},
			{"cline"},
			{"date"},
			{"dots"},
			{"emph", "em"},
			{"end"},
			{"eqref", "er", "eref"},
			{"footnote"},
			{"hat"},
			{"href"},
			{"hspace"},
			{"hspace"},
			{"include"},
			{"index"},
			{"input"},
			{"input"},
			{"intertext"},
			{"intertext", "inter"},
			{"invNr"},
			{"kapazitaet"},
			{"kosten"},
			{"label"},
			{"Large"},
			{"LaTeX"},
			{"LaTeX"},
			{"masse"},
			{"mathbb"},
			{"mathcal", "cal"},
			{"mathclap"},
			{"mathclap", "clap"},
			{"mathfrak"},
			{"mathfrak", "frak"},
			{"mathit", "mit"},
			{"mathrm", "mrm"},
			{"mbox"},
			{"mbox"},
			{"mnr"},
			{"name"},
			{"oarroweq"},
			{"operatorname"},
			{"operatorname"},
			{"operatorname", "op"},
			{"overline"},
			{"overline", "overl"},
			{"pageref"},
			{"paragraph", "par"},
			{"partial"},
			{"perm"},
			{"reflectbox"},
			{"ref", "r"},
			{"section", "sec"},
			{"setv"},
			{"sqrt"},
			{"sqrt", "sq"},
			{"subsection", "subsec", "subs"},
			{"substack"},
			{"substack", "subst"},
			{"subsubsection", "ssubsec", "ssubs"},
			{"tag"},
			{"textbf", "bf", "bold"},
			{"textit"},
			{"textsc", "sc"},
			{"textsl", "sl"},
			{"texttt", "tt"},
			{"text", "tx"},
			{"thispagestyle"},
			{"tilde"},
			{"tilde", "til"},
			{"title"},
			{"underbrace"},
			{"underline"},
			{"underline", "underl"},
			{"underset"},
			{"usepackage", "use", "usepac", "pac"},
			{"vspace"},
			{"vspace"},
			{"widehat", "what"},
			{"widetilde"},
			{"widetilde", "wtil"},
			{"xrightarrow"},
		},
		gen = function(trig)
			local expanded = "\\"..trig[1].."{"
			for _, alias in pairs(trig) do
				local nodes = {t(expanded), vi(1), t("}"),}
				table.insert(snippets, s({trig=alias}, nodes))
			end
		end
	},
	tex_arg2 = {
		trigs = {
			{"binom", "bin"},
			{"dfrac"},
			{"frac", "fr", "//", "/"},
			{"href"},
			{"markboth"},
			{"newcommand", "newc"},
			{"overset"},
			{"psfrag"},
			{"renewcommand", "renewc"},
			{"setcounter"},
			{"setlength"},
			{"tfrac"},
			{"underset"},
		},
		gen = function(trig)
			local expanded = "\\"..trig[1].."{"
			for _, alias in pairs(trig) do
				local nodes = {t(expanded), vi(1), t("}{"), i(2), t("}"),}
				table.insert(snippets, s({trig=alias}, nodes))
			end
		end
	},
	tex_envs = {
		trigs = {
			{"align*", "align"},
			{"alignat"},
			{"aufgabe", "auf"},
			{"center"},
			{"corollary", "cor", "korollar", "kor"},
			{"definition", "def", "dfn"},
			{"displaymath"},
			{"equation*", "eqnn"},
			{"equation", "eqq", "eqn"},
			{"example", "ex", "beispiel", "bsp"},
			{"flushleft", "flushl"},
			{"flushright", "flushr"},
			{"folgerung", "folg"},
			{"frame"},
			{"lemma", "lem"},
			{"loesung", "loes"},
			{"pmatrix"},
			{"proof", "prf", "beweis", "bew"},
			{"proposition", "prop"},
			{"quotation"},
			{"quote"},
			{"remark", "rem", "bemerkung", "bem"},
			{"split"},
			{"tabbing"},
			{"theorem", "satz", "thm"},
			{"verbatim"},
		},
		gen = function(trig)
			local expanded_begin = "\\begin{"..trig[1].."}"
			local expanded_end = "\\end{"..trig[1].."}"
			for _, alias in pairs(trig) do
				local nodes = {
					t({expanded_begin, ""}),
					vi(1, {indent="  "}),
					t({"", expanded_end}),
				}
				table.insert(snippets, s({trig=alias}, nodes))
			end
		end
	},
	tex_subsup = {
		trigs = {},
		gen = function(trig)
			table.insert(snippets, s({trig="\\"..trig}, {t("\\"..trig.."_{"), vi(1), t("}^{"), i(2), t("}")}))
		end,
	},
	tex_supsub = {
		trigs = {
			"nabla",
		},
		gen = function(trig)
			table.insert(snippets, s({trig="\\"..trig}, {t("\\"..trig.."^{"), vi(1), t("}_{"), i(2), t("}")}))
		end,
	},
	tex_sub = {
		trigs = {
			"id",
			"Id",
			"oint",
		},
		gen = function(trig)
			table.insert(snippets, s({trig="\\"..trig}, {t("\\"..trig.."_{"), vi(1), t("}")}))
		end,
	},
	tex_sup = {
		trigs = {"C", "e", "F", "H", "K", "N", "Q", "R", "Z"},
		gen = function(trig)
			local nodes = {t("\\"..trig.."^{"), vi(1), t("}")}
			table.insert(snippets, s({trig="\\"..trig}, nodes))
		end,
	},
}

for _, r in pairs(recipes) do
	local gen = r.gen
	for _, t in pairs(r.trigs) do
		gen(t)
	end
end

return snippets
