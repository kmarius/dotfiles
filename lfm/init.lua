local lfm = lfm

local fn = lfm.fn

local fm = lfm.fm
local chdir = fm.chdir

local ui = lfm.ui
local config = lfm.config
local map = lfm.map
local command = lfm.register_command
local log = lfm.log
local hook = lfm.register_hook
local spawn = lfm.spawn
local shell = lfm.shell
local compl = lfm.compl

local util = require("util")
local expr = util.expr

lfm.rifle.setup({
	config = "~/.config/lfm/rifle.conf",
})

config.ratios = {1, 3, 4}
config.truncatechar = '…'
config.preview = true
config.previewer = "~/.config/lfm/pv.sh"
config.inotify_blacklist = {"/mnt/sshfs/", "/mnt/smb/"}
config.inotify_timeout = 500
config.inotify_delay = 50

-- quickmarks are temporary, but we can set up some on startup
require("quickmarks").setup({
	quickmarks = {
		l = "~/Sync/programming/lfm",
		p = "/mnt/nfs/archpi/misc/videos",
	}
})

-- Overwrite builtin cd command to create and navigate to a temporary
-- directory with "cd t"
local function cd(dir)
	if dir == "t" then
		dir = shell.popen("mktemp -d")[1]
	end
	chdir(dir)
end
command("cd", cd, {compl=compl.limit(1, compl.dirs)})


local tabs = require("tabs")
map("q", tabs.close)
map("Q", lfm.quit)
map("<C-n>", tabs.new)
map("<Tab>", tabs.next)
map("<S-Tab>", tabs.prev)


local hasenv = util.hasenv
local function term()
	if hasenv("TMUX") then
		spawn({"tmux", "new-window", "fish", "-c", "set -U fx " .. shell.escape(lfm.sel_or_cur()), "-c", "fish"})
	elseif hasenv("WAYLAND_DISPLAY") then
		spawn({"foot", "fish", "-c", "set -U fx " .. shell.escape(lfm.sel_or_cur()), "-c", "fish"})
	else
		spawn({"urxvt", "-e", "fish", "-c", "set -U fx " .. shell.escape(lfm.sel_or_cur()), "-c", "fish"})
	end
end

local split_id = nil

local function tmux_splitv()
	spawn({"fish", "-c", "set -U fx $argv", unpack(lfm.sel_or_cur())})
	if hasenv("TMUX") then
		split_id = shell.popen({"tmux", "split-window", "-v", "-l", "40%", "-P"})[1]
	end
end

hook("ChdirPost", function()
	if split_id then
		spawn({"tmux", "send-keys", "-t", split_id, " cd "..fn.getpwd(), "Enter", "C-l"}, {err=false})
	end
end)

map("T", term)
map(";", tmux_splitv)
map("<A-t>", shell.sh("export LF_LEVEL=; foot lfm"), {desc="preview"})


local function fish_selection_set()
	spawn({"fish", "-c", "set -U fx $argv", unpack(lfm.sel_or_cur())})
end
map(",", fish_selection_set)


local function preview_pager()
	local file = fm.current_file()
	if file then
		shell.sh(string.format("%s %s | less -R", shell.escape(config.previewer), shell.escape(file)))()
	end
end
map("ii", preview_pager, {desc="preview"})

map("R", fm.reload, {desc="reload"})


do
	local logfile = lfm.config.logpath

	---Kill the terminal showing logs of the current instance.
	function log.kill()
		spawn({"pkill", "-f", "tail.*-f.*"..logfile})
	end

	---Open the log for the current instance in a terminal.
	function log.show()
		spawn({"foot", "tail", "-f", logfile}, {err=false, out=false})
		hook("ExitPre", log.kill)
	end
end
map("^", log.show)


local function gdb()
	spawn({"foot", "sudo", "gdb", "--pid", lfm.fn.getpid()})
end
command("gdb", gdb)


---Open a pager showing all lfm messages.
local function messages_pager()
	shell.execute(string.format([[printf -- '%%s\n' %s | less -R]], shell.escape(ui.messages())))
end
map("M", messages_pager)
command("messages", messages_pager)


map("dU", shell.sh("trash-restore"), {desc="trash-restore"})
map("du", shell.sh("du --max-depth=1 -h --apparent-size | $PAGER"), {desc="size (du)"})


local glob_to_pattern = require("glob").glob_to_pattern
local basename = require("util").basename

local function sel_or_glob(...)
	local globs = {...}
	local selection = {}
	if #globs > 0 then
		for i, glob in ipairs(globs) do
			globs[i] = glob_to_pattern(glob)
		end
		for _, file in ipairs(fm.current_dir().files) do
			for _, glob in pairs(globs) do
				if string.match(basename(file), glob) then
					table.insert(selection, "./"..basename(file))
					break
				end
			end
		end
		if #selection == 0 then
			return nil
		end
	else
		for _, file in ipairs(fm.selection_get()) do
			table.insert(selection, "./"..basename(file))
		end
		fm.selection_set()
	end
	return selection
end

-- really only works if the selected files are in the current directory
-- also don't select directories
local function bulkrename(...)
	local selection = sel_or_glob(...)
	if selection then
		lfm.execute({"vidir", unpack(selection)})
	else
		print("no files")
	end
end
map("B", bulkrename, {desc="bulkrename with vidir"})
command("bulkrename", bulkrename, {desc="bulkrename with vidir", tokenize=true})


command("chmod", shell.bash([[chmod "$1" -- "${files[@]}"]], {files=shell.ARRAY, fork=true}))
map("+x", expr("chmod +x"), {desc="chmod +x"})
map("-x", expr("chmod -x"), {desc="chmod -x"})

-- map("<c-c>", function() print("yoo") end)
-- map("<c-c>", function() lfm.execute({"notify-send",  "a", "b"}) end)


command("perms",
shell.sh("find . \\( -type d -exec chmod 755 {} \\; \\) -o \\( -type f -exec chmod 644 {} \\; \\)"),
{desc="Set standard permissions recursively"})


---Open files in vim in a new tmux pane. If no files are provided as arguments,
---currently selected files are opened.
---@varargs string files
local function vim(...)
	local args = {...}
	if #args > 0 then
		shell.tmux('nvim '..shell.escape(args))()
	else
		shell.tmux('nvim', {files=shell.ARGV})()
	end
end
command("v", vim, {tokenize=true, compl=compl.files})


local diff = shell.bash([[nvim -d -- "${files[@]}"]], {files=shell.ARRAY})
command("d", diff, {compl=compl.files})


local zip = shell.bash([[
set -f
mkdir "$1"
cp -r "${files[@]}" "$1"
zip -r "$1".zip "$1"
rm -rf "$1"
]],
{files=shell.ARRAY})
command("zip", zip, {tokenize=false})

local zstd = shell.bash([[
set -f
mkdir "$1"
cp -r "${files[@]}" "$1"
tar --zstd -cf "$1".zip "$1"
rm -rf "$1"
]],
{files=shell.ARRAY})
command("zstd", zstd, {tokenize=false})


---Map a "go-to" command.
---@param key string Key (sequence) without the leading "g"
---@param location string The destination.
local function gmap(key, location)
	map("g"..key, function() cd(location) end, {desc="cd "..location})
end

gmap("a", "~/Sync/Aktuelles")
gmap("b", "~/.local/bin")
gmap("c", "~/.config")
gmap("d", "~/Data")
gmap("l", "~/.local/share")
gmap("N", "~/.config/nvim")
gmap("L", "~/.config/lfm")
gmap("P", "~/.stshares/pixel-home")
gmap("r", "/run/user/1000/lfm")
gmap("s", "~/Sync")
gmap("S", "~/.stshares")
gmap("t", "/run/media/marius")
gmap("u", "~/.stshares/Uni")

local function chain(f, args, opts)
	args = args or {}
	opts = opts or {}
	local co
	opts.callback = function(ret) coroutine.resume(co, ret) end
	opts.fork = true
	co = coroutine.create(function()
		for _, arg in ipairs(args) do
			local cmd = f(arg)
			if cmd then
				shell.execute(cmd, opts)
				if coroutine.yield(co) ~= 0 and opts.errexit then
					return
				end
			end
		end
	end)
	coroutine.resume(co)
end

local dirent = require("posix.dirent")
local stat = require("posix.sys.stat")
local unistd = require("posix.unistd")

-- clean up leftover log files and fifos in case of a crash
local function cleanup()
	local rundir = lfm.config.runtime_dir
	local pids = {}
	for file in dirent.files(rundir) do
		local pid = string.match(file, "^(%d+)%.fifo$")
		if pid then
			pids[pid] = true
		end
	end
	for file in dirent.files("/tmp") do
		local pid = string.match(file, "^lfm%.(%d+)%.log$")
		if pid then
			pids[pid] = true
		end
	end
	for pid, _ in pairs(pids) do
		local logfile = "/tmp/lfm."..pid..".log"
		local fifo = rundir.."/"..pid..".fifo"
		if not stat.stat("/proc/"..pid) then
			spawn({"rm", "-f", logfile, fifo})
		else
			local target = unistd.readlink("/proc/"..pid.."/exe")
			if target and not string.match(target, ".*/lfm$") then
				spawn({"rm", "-f", logfile, fifo})
			end
		end
	end
end
command("clean", cleanup)

function lfm.delay(d)
	local co = coroutine.running()
	if co then
		if d > 0 then
			lfm.schedule(function() coroutine.resume(co) end, d)
			coroutine.yield()
		end
	else
		lfm.error("lfm.delay() called outside of a coroutine")
	end
end

local function co(f)
	return function() return coroutine.wrap(f)() end
end

function pprint(t)
	local lines = {}
	for key, value in pairs(t) do
		table.insert(lines, key..": ".. value)
	end
	shell.execute(string.format([[printf -- '%%s\n' %s | less -R]], shell.escape(lines)))
end

command("dircache_stats", function()
	pprint(lfm.dircache_stats())
end)

command("delete", function()
	spawn({"rm", "-rf", "--", unpack(lfm.sel_or_cur())})
	lfm.fm.selection_set()
end)

-- example opener

local opener_rules = {
	mime = {
		["^text/"] = {'$EDITOR "$@"', fork=false}
	},
	extension = {
		["%.mp4$"] = {'mpv "$@"', fork=true}
	},
}

local function open()
	local file = lfm.fm.open()
	if file then
		local files = fm.selection_get()
		if #files == 0 then
			files = {file}
		end
		file = files[1]
		local mime = lfm.fn.mime(file)
		for pat, c in pairs(opener_rules.mime or {}) do
			if string.match(mime, pat) then
				shell.execute({"sh", "-c", c[1], "_", unpack(files)}, {fork=c.fork})
				return
			end
		end
		for pat, c in pairs(opener_rules.extension or {}) do
			if string.match(file, pat) then
				shell.execute({"sh", "-c", c[1], "_", unpack(files)}, {fork=c.fork})
				return
			end
		end
		lfm.execute({"xdg-open", unpack(files)})
	end
end

-- overwrite default opener (xdg-open)
-- command("open", open)
